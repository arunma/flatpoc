import net.sf.flatpack.DataSet;
import net.sf.flatpack.DefaultParserFactory;
import net.sf.flatpack.Parser;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by Arun Manivannan on 5/16/15.
 */
public class FlatMain {


    public static void main(String[] args) {

        Parser parser = null;  //txt file to parse
        try {
            parser = DefaultParserFactory.getInstance().newFixedLengthParser(
                    new FileReader("/Users/Gabriel/Dropbox/arun/ScalaDataAnalysis/Code/FlatPoc/src/main/resources/map.fpmap.xml"), //fixed with column map
                    new FileReader("/Users/Gabriel/Dropbox/arun/ScalaDataAnalysis/Code/FlatPoc/src/main/resources/DataFile.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //obtain DataSet
        DataSet ds = parser.parse();

        while (ds.next()){ //loop through file
            System.out.println(ds.getString("FIRSTNAME"));
        }

    }
}
